namespace Utilities
{
    using System;
    /// <summary>
    /// Design and Architecture: Mohammad Ashraful Alam [ashraf@mvps.org]
    /// </summary>
    public sealed class Keys
    {
        Keys() { }
        public const string ConstConnectionStringKey = "EmployeeDB";
    }
}